mod protocol;
mod server;

fn main() -> protocol::result::Result<()> {
    let qname = "google.com";
    let qtype = protocol::query_type::QueryType::A;

    let server = ("8.8.8.8", 53);

    let socket = std::net::UdpSocket::bind(("0.0.0.0", 43210))?;

    let mut packet = protocol::dns_packet::DnsPacket::new();

    packet.header.id = 6666;
    packet.header.questions = 1;
    packet.header.recursion_desired = true;
    packet
        .questions
        .push(protocol::dns_question::DnsQuestion::new(qname.to_string(), qtype));

    let mut req_buffer = protocol::byte_packet_buffer::BytePacketBuffer::new();
    packet.write(&mut req_buffer)?;

    socket.send_to(&req_buffer.buf[0..req_buffer.pos], server)?;

    let mut res_buffer = protocol::byte_packet_buffer::BytePacketBuffer::new();
    socket.recv_from(&mut res_buffer.buf)?;

    let res_packet = protocol::dns_packet::DnsPacket::from_buffer(&mut res_buffer)?;
    println!("{:#?}", res_packet.header);

    for q in res_packet.questions {
        println!("{:#?}", q);
    }
    for rec in res_packet.answers {
        println!("{:#?}", rec);
    }
    for rec in res_packet.authorities {
        println!("{:#?}", rec);
    }
    for rec in res_packet.resources {
        println!("{:#?}", rec);
    }

    Ok(())
}
