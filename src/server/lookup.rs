use crate::protocol::query_type::QueryType;
use crate::protocol::dns_packet::DnsPacket;
use crate::protocol::dns_question::DnsQuestion;
use crate::protocol::byte_packet_buffer::BytePacketBuffer;

#[allow(dead_code)]
pub fn lookup(qname: &str, qtype: QueryType) -> crate::protocol::result::Result<DnsPacket> {
    // Forward queries to Google's public DNS
    let server = ("8.8.8.8", 53);

    let socket = std::net::UdpSocket::bind(("0.0.0.0", 43210))?;

    let mut packet = DnsPacket::new();

    packet.header.id = 6666;
    packet.header.questions = 1;
    packet.header.recursion_desired = true;
    packet
        .questions
        .push(DnsQuestion::new(qname.to_string(), qtype));

    let mut req_buffer = BytePacketBuffer::new();
    packet.write(&mut req_buffer)?;
    socket.send_to(&req_buffer.buf[0..req_buffer.pos], server)?;

    let mut res_buffer = BytePacketBuffer::new();
    socket.recv_from(&mut res_buffer.buf)?;

    DnsPacket::from_buffer(&mut res_buffer)
}